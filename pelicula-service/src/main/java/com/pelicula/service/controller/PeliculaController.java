package com.pelicula.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pelicula.service.entity.Pelicula;
import com.pelicula.service.service.PeliculaService;

import lombok.RequiredArgsConstructor;

@RequestMapping(PeliculaController.PELICULA)
@RestController
@RequiredArgsConstructor
public class PeliculaController {
	public static final String PELICULA = "/pelicula";

	@Autowired
	private PeliculaService peliculaService;

	@PostMapping("/save")
	public ResponseEntity<Pelicula> guardar(@RequestBody Pelicula pelicula){
		return new ResponseEntity<>(peliculaService.guardar(pelicula),HttpStatus.CREATED);
	}

	@GetMapping("/all")
	public ResponseEntity<List<Pelicula>> listar(){
		return new ResponseEntity<List<Pelicula>>(peliculaService.listar(),HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Pelicula> buscarPorId(@PathVariable Long id) {
		return new ResponseEntity<>(peliculaService.buscarPorCodigo(id),HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Pelicula> actualizarPorId(@PathVariable Long id, @RequestBody Pelicula pelicula) {
		return new ResponseEntity<>(peliculaService.actualizar(id, pelicula),HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity borrar(@PathVariable Long id) {
		peliculaService.borrar(id);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@GetMapping("/cliente/{id}")
	public ResponseEntity<List<Pelicula>> listarPorClienteID(@PathVariable Long id){
		return new ResponseEntity<List<Pelicula>>(peliculaService.listarPorclienteId(id),HttpStatus.OK);
	}
	
	
}
