package com.pelicula.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pelicula.service.entity.Pelicula;

@Repository
public interface PeliculaRepository extends JpaRepository<Pelicula, Long>{
	
	/**
	 * busca laspeliculas por el atributo clientId
	 * @param clienteId
	 * @return una lista de peliculas que pertenecen al clientID
	 */
	public List<Pelicula> findByClienteId(Long clienteId);

}
