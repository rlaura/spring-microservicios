package com.pelicula.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pelicula.service.entity.Pelicula;
import com.pelicula.service.repository.PeliculaRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PeliculaService {

	@Autowired
	private PeliculaRepository peliculaRepository;
	//R
	public List<Pelicula> listar() {
		return peliculaRepository.findAll();
	}
	//C - U
	public Pelicula guardar(Pelicula cliente) {
		return peliculaRepository.save(cliente);
	}

	//C - U
	public Pelicula actualizar(Long id, Pelicula peliculaRequest) {
		Pelicula existePelicula = peliculaRepository.findById(id).get();
		existePelicula.setNombre(peliculaRequest.getNombre());
		existePelicula.setDuracion(peliculaRequest.getDuracion());
		existePelicula.setClienteId(peliculaRequest.getClienteId());
		return peliculaRepository.save(existePelicula);
	}

	//R
	public Pelicula buscarPorCodigo(Long id) {
		// usar getReferenceById y no getById(ID) porque esta deprecado
		//usamos getById cuando usamos una session entre ida y vuleta y es un metodo lazy
		//return clienteRepository.getReferenceById(id);
		return peliculaRepository.findById(id).get();
	}
	//D
	public void borrar(Long id) {
		peliculaRepository.deleteById(id);
	}
	
	/* TODO: MiCROSERVICIOS **************************************************/
	public List<Pelicula> listarPorclienteId(Long id) {
		return peliculaRepository.findByClienteId(id);
	}
}
