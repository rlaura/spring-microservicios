package com.client.service.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.client.service.entity.Cliente;
import com.client.service.models.Pelicula;
import com.client.service.service.ClienteService;

import lombok.RequiredArgsConstructor;

@RequestMapping(ClienteController.CLIENTES)
@RestController
@RequiredArgsConstructor
public class ClienteController {
	public static final String CLIENTES = "/clientes";

	@Autowired
	private ClienteService clienteService;

	@PostMapping("/save")
	public ResponseEntity<Cliente> guardar(@RequestBody Cliente cliente) {
		return new ResponseEntity<>(clienteService.guardar(cliente), HttpStatus.CREATED);
	}

	@GetMapping("/all")
	public ResponseEntity<List<Cliente>> listar() {
		return new ResponseEntity<List<Cliente>>(clienteService.listar(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Cliente> buscarPorId(@PathVariable Long id) {
		return new ResponseEntity<>(clienteService.getClientById(id), HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Cliente> actualizarPorId(@PathVariable Long id, @RequestBody Cliente cliente) {
		return new ResponseEntity<>(clienteService.actualizar(id, cliente), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity borrar(@PathVariable Long id) {
		clienteService.borrar(id);
		return new ResponseEntity(HttpStatus.OK);
	}

	/*************************************************************
	 * MICROSERVICIOS
	 ************************************************************/
	/**
	 * 
	 * @param clienteId
	 * @param pelicula
	 * @return
	 */
	@PostMapping("/pelicula/{clienteId}")
	public ResponseEntity<Pelicula> guardarPelicula(@PathVariable("clienteId") Long clienteId,
			@RequestBody Pelicula pelicula) {
		Pelicula nuevaPelicula = clienteService.savePelicula(clienteId, pelicula);
		// return ResponseEntity.ok(nuevaPelicula);
		// TODO: esta opcion tambien es valida
		return new ResponseEntity<>(clienteService.savePelicula(clienteId, pelicula), HttpStatus.CREATED);
	}

	@GetMapping("/pelicula/{id}")
	public ResponseEntity<List<Pelicula>> listarPeliculas(@PathVariable Long id) {
		Cliente usuario = clienteService.getClientById(id);
		if (usuario == null) {
			return ResponseEntity.notFound().build();
		}

		return new ResponseEntity<List<Pelicula>>(clienteService.listarPeliculas(id), HttpStatus.OK);
	}

	@GetMapping("/peliculas/usuario/{id}")
	public ResponseEntity<Map<String, Object>> listarPeliculasconUsuario(@PathVariable Long id) {

		return new ResponseEntity<Map<String, Object>>(clienteService.listarPeliculasConUsuario(id), HttpStatus.OK);
	}

}
