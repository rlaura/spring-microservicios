package com.client.service.entity;

import com.client.service.utils.TipoDocumento;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 100)
	private String codigoUnico;

	@Column(length = 100, nullable = false)
	private String nombre;

	@Column(length = 100)
	private String apellidos;

	@Column(nullable = false, length = 40)
	@Enumerated(value = EnumType.STRING)
	private TipoDocumento tipoDocumento;

	@Column(length = 50, nullable = false)
	private String numeroDocumento;

}
