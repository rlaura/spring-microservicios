package com.client.service.utils;

public enum TipoDocumento {
	DNI, PASAPORTE, CARNET_EXTRANJERIA
}