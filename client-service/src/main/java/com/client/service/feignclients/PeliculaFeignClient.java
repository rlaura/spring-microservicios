package com.client.service.feignclients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.client.service.models.Pelicula;

@FeignClient(name = "pelicula-service",url = "http://localhost:8002")
//@FeignClient(name = "pelicula-service")
//@RequestMapping(name = "/pelicula")
public interface PeliculaFeignClient {
	
	
	//lo de arriba no es igual a lo de abajo
	//@PostMapping(name = "/pelicula")
	
	//todo lo de abajo funciona
	//@PostMapping("/pelicula/save")
	@PostMapping(path = "/pelicula/save")
	public Pelicula guardar(@RequestBody Pelicula pelicula );
	
	@GetMapping("/pelicula/cliente/{id}")
	public List<Pelicula> getPeliculas(@PathVariable Long id);
}
