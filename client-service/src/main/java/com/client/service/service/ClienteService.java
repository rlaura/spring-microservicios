package com.client.service.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.client.service.entity.Cliente;
import com.client.service.feignclients.PeliculaFeignClient;
import com.client.service.models.Pelicula;
import com.client.service.repository.ClienteRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private PeliculaFeignClient peliculaFeignClient;
	
	
	/**************** NO MICROSERVICIO *************************************************/
	//R
	public List<Cliente> listar() {
		return clienteRepository.findAll();
	}
	//C - U
	public Cliente guardar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	//C - U
	public Cliente actualizar(Long id, Cliente clienteRequest) {
		Cliente existeCliente = clienteRepository.findById(id).get();
		existeCliente.setNombre(clienteRequest.getNombre());
		existeCliente.setApellidos(clienteRequest.getApellidos());
		existeCliente.setTipoDocumento(clienteRequest.getTipoDocumento());
		existeCliente.setNumeroDocumento(clienteRequest.getNumeroDocumento());
		return clienteRepository.save(existeCliente);
	}

	//R
	public Cliente getClientById(Long id) {
		// usar getReferenceById y no getById(ID) porque esta deprecado
		//usamos getById cuando usamos una session entre ida y vuleta y es un metodo lazy
		//return clienteRepository.getReferenceById(id);
		return clienteRepository.findById(id).get();
	}
	//D
	public void borrar(Long id) {
		clienteRepository.deleteById(id);
	}
	
	/*********** MICROSERVICIOS  ************************************************************/
	/**
	 * guarda la pelicula en el usuario id 
	 * @param id
	 * @param pelicula
	 * @return una nueva pelicula que tiene el usuario
	 */
	public Pelicula savePelicula(Long id, Pelicula pelicula) {
		pelicula.setClienteId(id);
		Pelicula nuevaPelicula = peliculaFeignClient.guardar(pelicula);
		return nuevaPelicula;
	}
	/**
	 * lista todas las peliculas con el id del usuario
	 * @param id
	 * @return una lista de peliculas
	 */
	public List<Pelicula> listarPeliculas(Long id) {
		List<Pelicula> listaPeliculas = peliculaFeignClient.getPeliculas(id);
		return listaPeliculas;
	}
	
	/**
	 * lista todas las peliculas con el id del usuario y el usuario
	 * @param id
	 * @return una lista de peliculas
	 */
	public Map<String, Object> listarPeliculasConUsuario(Long id) {
		Map<String, Object> data =new HashMap<>();
		Cliente existeCliente = clienteRepository.findById(id).get();
		if(existeCliente == null) {
			data.put("Mensaje", "El usuario no existe");
			return data;
		}
		data.put("cliente", existeCliente);
		// aca falta validadores para ver si tiene peliculas
		List<Pelicula> listaPeliculas = peliculaFeignClient.getPeliculas(id);
		data.put("peliculas", listaPeliculas);
		
		return data;
	}
	
}
