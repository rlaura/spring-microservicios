package com.client.service.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pelicula {

	private String nombre;

	private String duracion;
	
	private Long clienteId;
	
}
